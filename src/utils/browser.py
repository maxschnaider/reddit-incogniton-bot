import time
import random
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from ast import literal_eval
from src.utils.kameleo import Kameleo
from src.models.profile import Profile


class Browser:
    def __init__(self, profile: Profile):
        self.profile = profile
        self.kameleo = Kameleo()
        self.kameleo.client.start_profile(profile.id)
        options = webdriver.ChromeOptions()
        options.add_experimental_option("kameleo:profileId", profile.id)
        self.browser = webdriver.Remote(
            command_executor=self.kameleo.webdriver_url,
            options=options
        )
        self.wait = WebDriverWait(self.browser, 2)

    @property
    def current_url(self) -> str:
        return self.browser.current_url

    def open(self, url, sleep=0):
        self.browser.get(url)
        self.remove_media_elements()
        time.sleep(sleep)

    def element_exists(self, selector, by=By.ID):
        try:
            return self.wait.until(EC.element_to_be_clickable((by, selector)))
        except Exception:
            pass

    def find_elements(self, selector, by=By.ID):
        try:
            if self.element_exists(selector, by):
                return self.browser.find_elements(by, selector)
        except Exception:
            pass

    def find_element(self, selector, by=By.ID):
        try:
            if self.element_exists(selector, by):
                return self.browser.find_element(by, selector)
        except Exception:
            pass

    def click(self, element, by=By.ID, sleep=0) -> bool:
        try:
            self.element_exists(element, by).click()
            time.sleep(sleep)
            return True
        except Exception:
            pass

    def type(self, text, input, by=By.ID, clear=True) -> bool:
        try:
            input_element = self.element_exists(input, by)
            if clear:
                input_element.clear()
            input_element.send_keys(text)
            return True
        except Exception:
            pass

    def scroll(self, y, sleep=0):
        self.browser.execute_script(f"window.scrollTo(0, {y})")
        time.sleep(sleep)

    def random_scroll(self):
        for _ in range(random.randint(5, 10)):
            self.scroll(y=random.randint(200, 600), sleep=random.randint(1, 2))

    def refresh(self, sleep=0):
        self.browser.refresh()
        time.sleep(sleep)

    def remove_media_elements(self):
        self.browser.execute_script(
            "return [].forEach.call(document.getElementsByTagName('iframe'), function (el) { el.remove(); });")
        self.browser.execute_script(
            "return [].forEach.call(document.getElementsByTagName('video'), function (el) { el.remove(); });")
        self.browser.execute_script(
            "return [].forEach.call(document.getElementsByTagName('img'), function (el) { el.remove(); });")

    def exit(self):
        self.kameleo.stop_profile(self.profile)

