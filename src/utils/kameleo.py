from ast import literal_eval
import requests
from src.models.profile import Profile
import time
import os
from kameleo.local_api_client.kameleo_local_api_client import KameleoLocalApiClient
from kameleo.local_api_client.models import SaveProfileRequest, ProfilePreview, StatusResponse


class Kameleo:
    def __init__(self):
        self.port = 5050
        self.url = f'http://localhost:{self.port}'
        self.webdriver_url = f'{self.url}/webdriver'
        self.client = KameleoLocalApiClient(self.url)

    def get_all_profiles(self, only_aged=True) -> [Profile]:
        profiles_data_list: [ProfilePreview] = self.client.list_profiles()
        profiles = []
        for profile_data in profiles_data_list:
            profile = Profile(profile_data)

            if only_aged and profile.age < 6:
                continue

            profiles.append(profile)
                
        return profiles

    def get_profiles_by_groups(self, groups) -> [Profile]:
        all_profiles = self.get_all_profiles()
        profiles = []
        if len(groups) == 0:
            profiles = all_profiles
        else:
            for profile in all_profiles:
                for group in groups:
                    if group in profile.group.split(' ')[0]:
                        profiles.append(profile)
        return profiles

    def stop_profile(self, profile: Profile, sleep=0):
        try:
            self.client.stop_profile(profile.id)
            self.client.save_profile(profile.id, body=SaveProfileRequest(path=profile.filepath))
            self.client.delete_profile(profile.id)
            time.sleep(sleep)
        except Exception as e:
            pass

    # def update_profile(self, profile: Profile):
    #     requests.post(self.url + 'update', data=profile.data)
