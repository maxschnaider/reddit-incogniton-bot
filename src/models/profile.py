import pathlib
from datetime import datetime, timezone


class Profile:
    def __init__(self, data=None):
        self.id = data.id if data else '0'
        self.name = data.last_known_path.split('\\')[-1] if data else 'Noname'
        self.filepath = f'C:\\Users\\Trainer\\Documents\\1morekameleo\\{self.name}'
        self.done = False
        self.upvoted = False
        self.banned = False

    @property
    def low_data_mode(self):
        return 'true' in self.data['profileData']['Other']['custom_browser_args_enabled']

    @low_data_mode.setter
    def low_data_mode(self, turn_on):
        if turn_on:
            self.data['profileData']['Other']['custom_browser_args_enabled'] = 'true'
            self.data['profileData']['Other']['custom_browser_args_string'] = '--blink-settings=imagesEnabled=false'

    @property
    def age(self):
        fname = pathlib.Path(self.filepath)
        if fname.exists():
            ctime = datetime.fromtimestamp(fname.stat().st_ctime, tz=timezone.utc).replace(tzinfo=None)
            age = datetime.utcnow() - ctime
            print(f"{self.name} age is {age.days} days")
            return age.days
        else:
            return 10
