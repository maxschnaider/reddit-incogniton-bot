from src.scripts.script_ban import ScriptBan
import pynput
import os
import sys


def main():
    def on_press(key):
        if key == pynput.keyboard.Key.esc:
            os.abort()
    keyboard_listener = pynput.keyboard.Listener(on_press=on_press)
    keyboard_listener.start()

    thread_number = int(sys.argv[1]) if len(
        sys.argv) > 1 else 15

    ScriptBan(thread_number).start()


if __name__ == "__main__":
    main()
