# Kameleo + Reddit Upvote Bot

## Installation

```bash
  git clone https://gitlab.com/maxschnaider/reddit-kameleo-bot.git
  cd reddit-kameleo-bot
  py -m pip install -r requirements.txt
```

## Running

Upvote script:
```bash
  # Before run start Kameleo app
  # REDDIT_POST_URL (example: https://www.reddit.com/r/subreddit/comments/post/postName/)
  # XXX: total number of upvotes (example: 80)
  # [YYY]: number of threads (optional, default is 15)
  py upvote.py REDDIT_POST_URL XXX [YYY]
```

Check banned accs script:
```bash
  # Before run start Kameleo app
  # [YYY]: number of threads (optional, default is 15)
  py ban.py [YYY]
```

## Слава Україні!

рускій карабль, іди нахуй!
