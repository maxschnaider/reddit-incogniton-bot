from src.scripts.script_upvote import ScriptUpvote
import pynput
import os
import sys



def main():
    def on_press(key):
        if key == pynput.keyboard.Key.esc:
            os.abort()
    keyboard_listener = pynput.keyboard.Listener(on_press=on_press)
    keyboard_listener.start()

    reddit_post_url = sys.argv[1] if len(
        sys.argv) > 1 else 'https://www.reddit.com/r/onlyfansgirls101/comments/v0d5uw/am_i_able_to_smile_with_my_eyes_more_on_my_free_of/'
    # groups = sys.argv[2].replace('groups=', '').split(
    #     ',') if len(sys.argv) > 2 else []
    upvotes_number = int(sys.argv[2]) if len(
        sys.argv) > 2 else 0
    thread_number = int(sys.argv[3]) if len(
        sys.argv) > 3 else 15

    ScriptUpvote(reddit_post_url, upvotes_number, thread_number).start()


if __name__ == "__main__":
    main()
